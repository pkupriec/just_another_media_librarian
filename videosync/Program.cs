﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace videosync
{
    class Program
    {
        static void Main(string[] args)
        {
            string sourcePath = "H:\\";//args[0];
            string destRootPath = "D:\\video";//args[1];
            ProcessDirectory(sourcePath, destRootPath);
            Console.ReadLine();

        }

        public static void ProcessDirectory(string targetDirectory, string destRootPath)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
                ProcessFile(fileName, destRootPath);

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
                ProcessDirectory(subdirectory, destRootPath);
        }

        // Insert logic for processing found files here.
        public static void ProcessFile(string path, string destRootPath)
        {
            DateTime creation = File.GetCreationTime(path);
            string filename = Path.GetFileName (path);
            Console.WriteLine("Processed file '{0}', creation date {1}", path,creation.ToString("M-d-yyyy"));
            if (filename.Contains(".MP4")||filename.Contains(".JPG") || filename.Contains(".JPEG"))
            {
                if (Directory.GetDirectories(destRootPath, creation.ToString("M-d-yyyy")).Length == 0)
                {
                    Directory.CreateDirectory(destRootPath + "\\" + creation.ToString("M-d-yyyy"));
                }
                File.Copy(path, destRootPath + "\\" + creation.ToString("M-d-yyyy") + "\\" + filename);
            }
        }
    }
}
